import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import PicrossHint from "@/components/PicrossHint.vue";

describe("Indices", () => {
    let componentWrapper;

    beforeEach(() => {
        componentWrapper = shallowMount(PicrossHint, {
            propsData: {
                target: "row",
                hints: [],
                currentState: [],
                showHelp: false,
            },
        });
    });
    it("Peut être assigné à une colonne", () => {
        componentWrapper.setProps({
            target: "column",
        });
        expect(componentWrapper.vm.isColumnHint).to.be.true;
    });
    it("Peut être assigné à une ligne", () => {
        componentWrapper.setProps({
            target: "row",
        });
        expect(componentWrapper.vm.isColumnHint).to.be.false;
    });
    it("Doit être marqué lorsque la réponse est valide", () => {
        componentWrapper.setProps({
            hints: [1,2],
            currentState: [1,0,0,1,1],
        });
        expect(componentWrapper.vm.isMarked).to.be.true;
    });
    it("Ne doit pas être marqué lorsque la réponse n'est pas valide", () => {
        componentWrapper.setProps({
            hints: [1,2],
            currentState: [1,1,0,1,1],
        });
        expect(componentWrapper.vm.isMarked).to.be.false;
    });
    it("Fonctionne lorsque l'indice est 0", () => {
        componentWrapper.setProps({
            hints: [0],
            currentState: [0,0,0,0,0,0,0,0,0],
        });
        expect(componentWrapper.vm.isMarked).to.be.true;
        componentWrapper.setProps({
            currentState: [1,0,0,0,0,0,0,0,0],
        });
        expect(componentWrapper.vm.isMarked).to.be.false;
    });
    it("Fonctionne lorsque la réponse consiste à marquer toute les cases", () => {
        componentWrapper.setProps({
            hints: [5],
            currentState: [1,1,1,1,1],
        });
        expect(componentWrapper.vm.isMarked).to.be.true;
        componentWrapper.setProps({
            currentState: [1,0,1,1,1],
        });
        expect(componentWrapper.vm.isMarked).to.be.false;
    });
    it("Est en erreur lorsque trop de cases ont été cochées", () => {
        componentWrapper.setProps({
            hints: [2,3,1],
            currentState: [1,1,0,1,1,1,0,1,1,0],
        });
        expect(componentWrapper.vm.isWrong).to.be.true;
    });
    it("N'affiche pas les erreurs quand l'affichage de l'aide est désactivée", () => {
        componentWrapper.setProps({
            showHelp: false,
            hints: [2,3,1],
            currentState: [1,1,1,1,1,1,0,1,1,0],
        });
        expect(componentWrapper.vm.isWrong).to.be.true;
        expect(componentWrapper.vm.classHint.error).to.be.false;
    });
    it("Affiche les erreurs quand l'affichage de l'aide est activée", () => {
        componentWrapper.setProps({
            showHelp: true,
            hints: [2,3,1],
            currentState: [1,1,1,1,1,1,0,1,1,0],
        });
        expect(componentWrapper.vm.isWrong).to.be.true;
        expect(componentWrapper.vm.classHint.error).to.be.true;
    });
    it("Ne marque pas les indices quand l'affichage de l'aide est désactivée", () => {
        componentWrapper.setProps({
            showHelp: false,
            hints: [2,3,1],
            currentState: [1,1,0,1,1,1,0,1,0],
        });
        expect(componentWrapper.vm.isMarked).to.be.true;
        expect(componentWrapper.vm.classHint.marked).to.be.false;
    });
    it("Marque les indices quand l'affichage de l'aide est activée", () => {
        componentWrapper.setProps({
            showHelp: true,
            hints: [2,3,1],
            currentState: [1,1,0,1,1,1,0,1,0],
        });
        expect(componentWrapper.vm.isMarked).to.be.true;
        expect(componentWrapper.vm.classHint.marked).to.be.true;
    });
});
